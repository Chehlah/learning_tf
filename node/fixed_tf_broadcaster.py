#!/usr/bin/env python  
import roslib
roslib.load_manifest('learning_tf')

import rospy
import tf
import math

if __name__ == '__main__':
	rospy.init_node('fixed_tf_broadcaster')
	br = tf.TransformBroadcaster()
	rate = rospy.Rate(10.0)
	# while not rospy.is_shutdown():
	# 	br.sendTransform((1.0, 0.0, 0.0),
	# 					 tf.transformations.quaternion_from_euler(0, 0, 0),
	# 					 rospy.Time.now(),
	# 					 "carrot1",
	# 					 "turtle1")

	# dynamic tf
	while not rospy.is_shutdown():
		t = rospy.Time.now().to_sec() * math.pi
		br.sendTransform((2.0 * math.sin(t), 2.0 * math.cos(t), 0.0),
						 (0.0, 0.0, 0.0, 1.0),
						 rospy.Time.now(),
						 "carrot1",
						 "turtle1")
		rate.sleep()